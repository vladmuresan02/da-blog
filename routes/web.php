<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'IndexController@welcome');




Route::get('/home', 'HomeController@index');
Route::get('/article/{article}', 'ArticlesController@article');
Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('/user/edit', 'UsersController@edit');
Route::post('/user/edit/change_name', 'UsersController@changeName');
Route::post('/user/edit/change_password', 'UsersController@changePassword');
Route::post('/user/edit/change_avatar', 'UsersController@changeAvatar');

