<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'avatar_path' => $faker->imageUrl(200,200),
    ];
});

$factory->define(App\Article::class, function (Faker\Generator $faker) {

    return [
        'title' => $faker->sentence,
        'body' => $faker->paragraph(11,20),
        'user_id' => 1,
    ];
});

$factory->define(App\Image::class, function (Faker\Generator $faker) {

    return [
        'type' => 'cover',
        'path' => $faker->imageUrl(500,200),
        'article_id' => 1,
    ];
});
