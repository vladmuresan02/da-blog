<?php

use \App\User;
use \App\Article;
use \App\Image;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

//        factory(User::class, 20)->create();
        $users = factory(User::class, 20)->create();
        $articles = factory(Article::class, 20)->create([]);
        $images = factory(Image::class, 1)->create();
    }
}
