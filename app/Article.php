<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    
    protected $fillable = [ 'title','body','user_id' ];

//    public function scopeAuthenticatedUser($query) {
//        return $query->whereUserId(auth()->user()->id);
//    }

    public function user(){
        return $this->belongsTo('App\User');
    }
    public function comments(){
        return $this->hasMany('App\Comment');
    }
    public function images(){
        return $this->hasMany('App\Image')->where('type', 'normal');
    }
    public function cover(){
        return $this->hasOne('App\Image')->where('type', 'cover');
    }
}
