<?php

namespace App\Http\Controllers;

use Illuminate\Http\File;
use Illuminate\Http\Request;

use App\Http\Requests;

class UsersController extends Controller
{
    /**
     * UsersController constructor.
     */
//    public function __construct()
//    {
//        $this->middleware('user');
//    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(){
        return view('users.edit');
    }

    /**
     * @param Request $request
     */
    public function changeName(Request $request){
      
        $this->validate($request, [
            'name' => 'required',
        ]);

        $user=auth()->user();
        $user->name = $request['name'];
        $user->update();

    }
    public function changePassword(Request $request){

        $this->validate($request, [
            'new-password' => 'required',
            'repeat-new-password' => 'required|same:new-password',
        ]);

        $user=auth()->user();
        $user->password = bcrypt($request['new-password']);
        $user->update();

    }

    /**
     * @param Request $request
     */
    public function changeAvatar(Request $request){

        $user=auth()->user();

        $avatar = time().'.'.$request['avatar-input']->getClientOriginalExtension();
        $request['avatar-input']->move(public_path('img/upload'),$avatar);
        \File::delete(public_path('img/upload').'/'.$user->avatar_path);
        $user->avatar_path = 'img/upload/'.$avatar;
        $user->update();
        return redirect()->back()->with('status','bn boss');

    }
}
