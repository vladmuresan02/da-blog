<?php

namespace App\Http\Controllers;
use App\Article;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


use App\Http\Requests;

class ArticlesController extends Controller
{
    use AuthenticatesUsers;

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function article($id)
    {
        $article = Article::find($id);


        if(Auth::user()) {
            $user=Auth::user();
        } else {
            $user="unauth";
        }




        return view('article', compact(['article', 'user']));
    }

    public function create(){

    }

    public function store(){


        //validator

        $request = request()->all();
        $request['user_id'] = auth()->user()->id;

        $article = Article::create([]);
        
        
    }

    public function edit(Article $article) {


    }

    public function update() {

    }

    public function destroy($article) {
        Article::destroy($article);
    }

}
