<?php

namespace App\Http\Controllers;

use App\Article;

use Illuminate\Http\Request;

use App\Http\Requests;

class IndexController extends Controller{
//{ public function __construct()
//{
//    $this->middleware('guest');
//}
    public function welcome()
    {
        $articles = Article::with('cover')->paginate(9);


        return view('welcome', compact('articles'));
    }
}
