@extends('layouts.layout')
@section('head')
    <link rel="stylesheet" href="{{asset('../css/fileinput.min.css')}}">

@endsection
@section('header')
    <nav class="navbar navbar-default" id="header">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img src="{{asset('../img/logo.png')}}" alt=""></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true" aria-expanded="false">{{auth()->user()->name}} <span
                                    class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/user/add">Add new article</a></li>
                            <li><a href="/home">View profile</a></li>
                            <li><a href="/index">See all articles</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ url('/logout')}}">Log out</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

@endsection

@section('content')
    <section id="content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="posts-heading">Welcome, {{auth()->user()->name}}. Below you can manage your profile.</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 side-menu-container">
                    <a href="{{url('/home/articles')}}" class="side-menu-element ">Articles</a>
                    <a href="{{url("/user/edit")}}" class="side-menu-element side-menu-element-active">Profile
                        settings</a>
                </div>
                <div class="col-md-10 card-big">
                    <h3>Change Name</h3>
                    <form action="/user/edit/change_name" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            {{--<label for="name">Change name</label>--}}

                            <input type="text" id="name" name="name"  value="{{auth()->user()->name}}" class="form-control">
                        </div>
                        <div class="form-group">
                                <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                    </form>
                    <h3>Change Password</h3>
                    <form action="/user/edit/change_password" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                                <label for="name">New Password</label>
                                <input type="password" id="new-password" name="new-password"  class="form-control">
                        </div> <div class="form-group">
                                <label for="name">Repeat new Password</label>
                                <input type="password" id="repeat-new-password" name="repeat-new-password"  class="form-control">
                        </div>
                        <div class="form-group">
                                <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                    </form>
                    <h3>Change Profile Picture</h3>
                    <img src="{{auth()->user()->avatar_path}}" alt="" class="img-responsive profile-image">
                    <form action="/user/edit/change_avatar" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                                <input type="file" name="avatar-input" id="avatar-input" accept="image/*">
                        </div>
                        <div class="form-group">
                                <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>

    </section>
@endsection
@section('custom-scripts')
    <script src="{{asset('../js/fileinput.min.js')}}"></script>
    <script>
        $("#avatar-input").fileinput({
            'showUpload':false,
        });
    </script>
@endsection

