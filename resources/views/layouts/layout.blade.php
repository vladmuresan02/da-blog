<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>dablog</title>
    <link rel="stylesheet" href="{{asset('../css/bootstrap.min.css')}}">
    @yield('head')
    <link rel="stylesheet" href="{{asset('../css/stylesheet.css')}}">

</head>
<body>
@yield('header')




@yield('content')

<section id="footer">
    <div class="container">
        <p class="pull-left footer-text">
            VLAD MURESAN
        </p>
        <div class="pull-right">
            <a href="" class="footer-text">Despre dablog</a>
            <a href="" class="footer-text">Contact</a>
        </div>
        <div class="clearfix"></div>
    </div>
</section>

<script src="{{asset('../js/jquery-1.12.4.min.js')}}">
</script>
<script src="{{asset('../js/bootstrap.min.js')}}">
</script>

@yield('custom-scripts')
</body>
</html>