@extends('layouts.layout')
@section('head')
    <link rel="stylesheet" href="{{asset('../css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" href="{{asset('../css/dataTables.bootstrap.min.css')}}">
@endsection
@section('header')
    <nav class="navbar navbar-default" id="header">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img src="{{asset('../img/logo.png')}}" alt=""></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true" aria-expanded="false">{{auth()->user()->name}} <span
                                    class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/user/add">Add new article</a></li>
                            <li><a href="/user/profile">View profile</a></li>
                            <li><a href="/home">See all articles</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ url('/logout')}}">Log out</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

@endsection

@section('content')
    <section id="content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="posts-heading">Welcome, {{auth()->user()->name}}. Below you can manage your profile.</h3>
                    </div>
            </div>
            <div class="row">
                <div class="col-md-2 side-menu-container    ">
                    <a href="{{url('/home/articles')}}" class="side-menu-element side-menu-element-active">Articles</a>
                    <a href="{{url('/user/edit')}}" class="side-menu-element">Profile settings</a>
                </div>
                <div class="col-md-10 card-big">
                    <table class="table table-striped" id="article-table">
                        <thead>
                        <th class="table-element">Title</th>
                        <th class="table-element">Actions</th>
                        </thead>
                        <tbody>
                        @foreach($articles as $article)
                            <tr>
                                <td class="table-element"><a href="/article/{{$article->id}}">{{$article->title}}</a></td>
                                <td class="table-element"><a href="">x</a></td>

                            </tr>

                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </section>
@endsection
@section('custom-scripts')
    <script src="{{asset('../js/jquery.dataTables.min.js')}}">

    </script>
    <script src="{{asset('../js/dataTables.bootstrap.js')}}">
    </script>
    <script>
        $(document).ready(function () {
            $('#article-table').DataTable();
        });
    </script>
@endsection
