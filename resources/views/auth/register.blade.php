@extends('layouts.layout')
@section('head')
    <link rel="stylesheet" href="{{asset('../css/fileinput.min.css')}}">
@endsection
@section('header')
    <nav class="navbar navbar-default" id="header">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-left">

                <a class="navbar-brand" href="/"><img src="{{asset('../img/logo.png')}}" alt=""></a>
            </div>
            <ul class=" navbar-right">
                <li class="">
                    <a class="menu-icon pull-right " href="#" id="menu-show">
                        <span class="glyphicon glyphicon-menu-hamburger "> </span>
                    </a>
                </li>
            </ul>

        </div>
        </div><!-- /.container-fluid -->
    </nav>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Register</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST"  action="{{ url('/register') }}" enctype="multipart/form-data" >
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Name</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name"
                                           value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control"
                                           name="password_confirmation" required>

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="avatar" class="col-md-4 control-label">Upload Profile Picture</label>
                                <div class="col-md-6">
                                    <input type="file" id="avatar-input" name="avatar-input" accept="image/*">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Register
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="menu">
        <div class="container">
            <div class="pull-right menu-icon menu-close"><a href="#" id="menu-hide"><span
                            class="glyphicon glyphicon-remove "> </span></a></div>
            <div class="clearfix"></div>

            <div class="text-center menu-link-container">
                <a href="/about" class="menu-link">ABOUT DABLOG</a>
                <a href="/contact" class="menu-link">CONTACT</a>
                <a href="/login" class="menu-link">LOG IN</a>
                <a href="/logout" class="menu-link">SIGN UP</a>
            </div>
        </div>
    </div>


@endsection
@section('custom-scripts')
    <script src="{{asset('../js/fileinput.min.js')}}"></script>
    <script>
        $(".menu").hide();
        $("#menu-show").show();

        $("#menu-show").click(function () {
            $(".menu").show(500);
            $("#menu-show").hide();
        });
        $("#menu-hide").click(function () {
            $(".menu").hide(500);
            $("#menu-show").show();
        });
    </script>
    <script>
        $("#avatar-input").fileinput({
            'showUpload':false,
        });
    </script>
@endsection

