@extends('layouts.layout')
@section('header')

    <nav class="navbar navbar-default" id="header">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-left">

                <a class="navbar-brand" href="/"><img src="{{asset('../img/logo.png')}}" alt=""></a>
            </div>
            <ul class=" navbar-right">
                <li class="">
                    <a class="menu-icon pull-right " href="#" id="menu-show">
                        <span class="glyphicon glyphicon-menu-hamburger "> </span>
                    </a>
                </li>
            </ul>

        </div>
        </div><!-- /.container-fluid -->
    </nav>

@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Login</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Login
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="menu">
        <div class="container">
            <div class="pull-right menu-icon menu-close"><a href="#" id="menu-hide"><span
                            class="glyphicon glyphicon-remove "> </span></a></div>
            <div class="clearfix"></div>

            <div class="text-center menu-link-container">
                <a href="/about" class="menu-link">ABOUT DABLOG</a>
                <a href="/contact" class="menu-link">CONTACT</a>
                <a href="/login" class="menu-link">LOG IN</a>
                <a href="/logout" class="menu-link">SIGN UP</a>
            </div>
        </div>
    </div>


@endsection
@section('custom-scripts')
    <script>
        $(".menu").hide();
        $("#menu-show").show();

        $("#menu-show").click(function () {
            $(".menu").show(500);
            $("#menu-show").hide();
        });
        $("#menu-hide").click(function () {
            $(".menu").hide(500);
            $("#menu-show").show();
        });
    </script>
@endsection
