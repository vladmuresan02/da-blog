<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('../css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('../css/stylesheet.css')}}">
    <title>Index | dablog </title>

</head>
<body>
<section id="header" class="wrapper">
    <div class="container">
        <div class="row wrapper-content">
            <div class="col-md-12">
                <div>
                    <img src="{{asset('../img/logo.png')}}" alt="" class="header-logo text-left"
                         style="display: inline-block">

                    <a class="menu-icon pull-right " href="#" id="menu-show">
                        <span class="glyphicon glyphicon-menu-hamburger "> </span>
                    </a>

                    {{--<div class="clearfix">--}}
                    {{--</div>--}}

                </div>
                <div class="clearfix"></div>
                <div class="heading-container col-md-6 col-md-offset-3">
                    <h1 class="text-center heading">
                        WRITE STORIES.
                    </h1>
                    <p class="text-center subheading">
                        Let everybody read about whatever you want.
                    </p>
                </div>
                @if(auth()->user())
                <div class="col-md-12">
                <h3 class="text-center subheading"> You are logged in as {{auth()->user()->name}}</h3>
                </div>


                    <div class="col-md-6 col-sm-8 col-xs-12 text-center col-md-offset-3 col-sm-offset-2 button-container">
                        <a class="button" href="/home">DASHBOARD</a>
                        <a class="button" href="/logout">LOG OUT</a>

                    </div>
                @else
                    <div class="col-md-6 col-sm-8 col-xs-12 text-center col-md-offset-3 col-sm-offset-2 button-container ">
                        <a class="button" href="/login">LOG IN</a>
                        <a class="button" href="/register">SIGN UP</a>
                    </div>
                @endif

            </div>
        </div>
    </div>

</section>
<section id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="posts-heading">POSTS BY PEOPLE</h3>
                <div class="separator">

                </div>
            </div>
        </div>
        @foreach($articles as $article)
            <div class=" col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-left: 0; ">
                <div class="card">
                    <div>
                        {{--@foreach($article->images as $image)--}}
                        {{--<img src="{{asset('img/upload/'.$image->path)}}" alt="">--}}
                        {{--@endforeach--}}
                        <img src="{{url($article->user->avatar_path)}}" alt=""
                             class="pull-left avatar">
                        <div class="post-info">
                            <h2 class="poster-name">{{$article->user->name}}</h2>
                            <h5 class="post-date">{{$article->created_at}}</h5>
                            <h5 class="post-comments"><span
                                        class="glyphicon glyphicon-comment  "></span> {{$article->comments->count()}}
                                @if($article->comments->count()==1)
                                    comment
                                @else
                                    comments
                                @endif
                            </h5>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div>
                        <a class="post-title" href="{{"/article/".$article->id}}">{{$article->title}} FDFGDSFGDSFGDSF
                            DSFDS FSA DSAF SDAF SA</a>
                        <img src="{{$article->cover == null ? '' : url($article->cover->path)}}"
                             alt=""
                             class="img-responsive post-cover"
                        >
                        <div class="post-excerpt">{{$article->body}}</div>


                        <a href="{{"/article/".$article->id}}" class="read-more">read more</a>
                    </div>
                    {{--@foreach( $article->comments as $comment)--}}
                    {{--<div>--}}
                    {{--<p>{{$comment->user->name}}</p>--}}
                    {{--<p>{{$comment->created_at}}</p>--}}
                    {{--<p>{{$comment->body}}</p>--}}
                    {{--</div>--}}
                    {{--@endforeach--}}
                </div>

            </div>
        @endforeach
    </div>
    <div class="container">
        <div class="separator"></div>
        {{ $articles->links('pagination') }}
    </div>
</section>
<div class="menu">
    <div class="container">
        <div class="pull-right menu-icon menu-close"><a href="#" id="menu-hide"><span
                        class="glyphicon glyphicon-remove "> </span></a></div>
        <div class="clearfix"></div>

        <div class="text-center menu-link-container">
            <a href="" class="menu-link">ABOUT DABLOG</a>
            <a href="" class="menu-link">CONTACT</a>
        </div>
    </div>
</div>
<section id="footer">
    <p class="text-center footer-text">
        VLAD MURESAN
    </p>
</section>
<script src="{{asset('../js/jquery-1.12.4.min.js')}}"></script>

<script>
    $(".menu").hide();
    $("#menu-show").show();

    $("#menu-show").click(function () {
        $(".menu").show(500);
        $("#menu-show").hide();
    });
    $("#menu-hide").click(function () {
        $(".menu").hide(500);
        $("#menu-show").show();
    });
</script>

</body>
</html>
