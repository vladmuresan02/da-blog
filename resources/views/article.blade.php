@extends('layouts.layout')

@section('header')
    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/ro_RO/sdk.js#xfbml=1&version=v2.8";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    @if(auth()->guest())
        <nav class="navbar navbar-default" id="header">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-left">

                    <a class="navbar-brand" href="/"><img src="{{asset('../img/logo.png')}}" alt=""></a>
                </div>
                <ul class=" navbar-right">
                    <li class="">
                        <a class="menu-icon pull-right " href="#" id="menu-show">
                            <span class="glyphicon glyphicon-menu-hamburger "> </span>
                        </a>
                    </li>
                </ul>

            </div>
            </div><!-- /.container-fluid -->
        </nav>
    @else
        <nav class="navbar navbar-default" id="header">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="{{asset('../img/logo.png')}}" alt=""></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">{{$user->name}} <span
                                        class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="/user/{{$user->id}}/add">Add new article</a></li>
                                <li><a href="/user/{{$user->id}}/profile">View profile</a></li>
                                <li><a href="/home">See all articles</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{{ url('/logout')}}">Log out</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    @endif
@endsection

@section('content')
    <section id="content">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="card-big" id="article">
                        <div>
                            {{--@foreach($article->images as $image)--}}
                            {{--<img src="{{asset('img/upload/'.$image->path)}}" alt="">--}}
                            {{--@endforeach--}}
                            <img src="{{$article->user->avatar_path}}" alt=""
                                 class="pull-left avatar">
                            <div class="post-info">
                                <h2 class="poster-name">{{$article->user->name}}</h2>
                                <h5 class="post-date">{{$article->created_at}}</h5>
                                <h5 class="post-comments"><span
                                            class="glyphicon glyphicon-comment  "></span> {{$article->comments->count()}}
                                    @if($article->comments->count()==1)
                                        comment
                                    @else
                                        comments
                                    @endif
                                </h5>
                            </div>
                        </div>

                        <div>
                            <h1 class="post-big-title">{{$article->title}}</h1>
                            <div class="separator" style="margin-left:10px"></div>
                            <div class="post-body">{{$article->body}}
                                {{$article->body}}
                            </div>
                            <div class="separator" style="margin-left:10px"></div>
                            <div class="social " style="margin-left:10px">
                                <div class="social-button">
                                    <div class="fb-share-button" data-layout="button"
                                         data-href="{{'/article/'.$article->id}}" data-size="small"
                                         data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank"
                                                                      href="https://www.facebook.com/sharer/sharer.php?u&amp;src=sdkpreparse">Distribuie</a>
                                    </div>
                                    <div class="social-button">
                                        <a class="twitter-share-button" style="margin-top:20px"
                                           href={{"https://twitter.com/intent/tweet?text=".str_slug($article->title) }}
                                                   data-size="small">
                                            Tweet</a>
                                    </div>

                                </div>
                            </div>


                        </div>

                    </div>
                    <div class="card-big" id="comments">
                        <h2 class="post-title"> Comments </h2>
                        @foreach($article->comments as $comment)
                            <div class="comment">
                                <img src="{{'/img/upload/'.$comment->user->avatar_path}}" alt=""
                                     class="avatar pull-left">
                                <h1 class="comment-author">{{$comment->user->name}} <span
                                            class="comment-date">{{$comment->created_at}}</span></h1>
                                <p class="comment-body">{{$comment->body}}</p>
                            </div>
                            <div class="clearfix"></div>
                            <hr>

                        @endforeach


                        {{--comment form--}}
                        @if($user==="unauth")
                            <p class="comment-body"><a href="/login">Login</a> to post comments</p>

                        @else
                            <form action="" method="POST" name="add_comment_form" id="add_comment_form" >
                                <div class="form-group">
                                    <textarea class="form-control" rows="3" name="comment" id="comment"></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="post-comment" >POST COMMENT</button>
                                </div>
                            </form>

                        @endif
                    </div>
                </div>
            </div>
    </section>


    @if($user==="unauth")
        <div class="menu">
            <div class="container">
                <div class="pull-right menu-icon menu-close"><a href="#" id="menu-hide"><span
                                class="glyphicon glyphicon-remove "> </span></a></div>
                <div class="clearfix"></div>

                <div class="text-center menu-link-container">
                    <a href="/about" class="menu-link">ABOUT DABLOG</a>
                    <a href="/contact" class="menu-link">CONTACT</a>
                    <a href="/login" class="menu-link">LOG IN</a>
                    <a href="/logout" class="menu-link">SIGN UP</a>
                </div>
            </div>
        </div>
    @endif
    {{--footer--}}
    @if($user==="unauth")

    @else

    @endif

@endsection

@section('custom-scripts')
    {{--scripts--}}
    @if($user==="unauth")

        <script>window.twttr = (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0],
                        t = window.twttr || {};
                if (d.getElementById(id)) return t;
                js = d.createElement(s);
                js.id = id;
                js.src = "https://platform.twitter.com/widgets.js";
                fjs.parentNode.insertBefore(js, fjs);

                t._e = [];
                t.ready = function (f) {
                    t._e.push(f);
                };

                return t;
            }(document, "script", "twitter-wjs"));</script>

        <script>
            $(".menu").hide();
            $("#menu-show").show();

            $("#menu-show").click(function () {
                $(".menu").show(500);
                $("#menu-show").hide();
            });
            $("#menu-hide").click(function () {
                $(".menu").hide(500);
                $("#menu-show").show();
            });
        </script>
    @endif
@endsection
